Rails.application.routes.draw do
  post '/auth/login', to: 'authentication#login'

  resources :users, only: [:index, :create]
  resources :doctors, only: [:index]
  resources :booking, only: [:index, :create]
  resources :schedule, only: [:index, :create]
  resources :hospitals, only: [:index]

  match '*path' => 'application#not_found', via: :all
end
