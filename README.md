## Config after clone

```
$ bundle install
```
```
$ rails db:setup
```

API

```
$ rails db:migrate RAILS_ENV=development
```
```
$ rails db:seed
```
```
$ rails s
```

RSPEC TEST

```
$ rails db:migrate RAILS_ENV=test
```
```
$ bundle exec rspec
```

## Route

* add/register user ``POST /users``
* display all user ``GET /users``
* display all doctor ``GET /doctors``
* display all hospital ``GET /hospitals``
* login ``POST /auth/login``

With AUTH

* display all schedule doctor ``GET /schedule``
* add schedule doctor ``POST /schedule``
* display all booking user with doctor ``GET /booking``
* add booking user with doctor ``POST /booking``


For more information about API Request, you have downloaded the POSTMAN collection in the folder.
Thanks