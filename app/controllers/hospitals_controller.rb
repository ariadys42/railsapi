class HospitalsController < ApplicationController
    def index
        @hospital = Hospital.all

        render json: @hospital.to_json
    end
end
