class ScheduleController < ApplicationController
    before_action :authorize_request

    def index
        @schedule = Schedule.all

        render json: @schedule.to_json(
            :include => {
              :doctor => {only: [:id, :name, :email]},
              :hospital => {only: [:id, :name, :address]}
            }, :except => [:doctor_id, :hospital_id])
    end

    def create
        required = [:hospital_id, :doctor_id]
        if required.all? {|k| params.has_key? k}
            checkHospital = Hospital.where(id: params[:hospital_id]).first
            checkDoctor = User.where(level: 2, id: params[:doctor_id]).first

            if checkDoctor && checkHospital
                @schedule = Schedule.new(schedule_params)

                if @schedule.save
                    render json: @schedule.to_json
                else
                    render json: { error: @schedule.errors }, status: 401
                end
            else
                render json: { error: "invalid data" }, status: 401
            end
        else
            render json: { error: "invalid data" }, status: 401
        end
    end

    private
    def schedule_params
        params.permit(:doctor_id, :hospital_id, :schedule_date)
    end

end
