class DoctorsController < ApplicationController
    def index
        @doctor = User.where(level: 2)

        render json: @doctor.to_json
    end
end
