class BookingController < ApplicationController
    before_action :authorize_request

    def index
        @booking = Booking.all

        render json: @booking.to_json(
        :include => {
          :user => {only: [:id, :name, :email]},
          :doctor => {only: [:id, :name, :email]},
          :hospital => {only: [:id, :name, :address]}
        }, :except => [:doctor_id, :user_id, :hospital_id])
    end

    def create
        cekScheduleBooking = Booking.where(booking_date: params[:booking_date], doctor_id: params[:doctor_id], user_id: params[:user_id], active: true).first
        scheduleSelect = Schedule.where(id: params[:schedule_id]).first
        countBooking = Booking.where(schedule_id: params[:schedule_id]).count
        checkUser = User.where(id: params[:user_id], level: 1).first

        if !scheduleSelect || !checkUser
            render json: {
                error: "invalid data",
            }, status: 401
        else
            trimDate = scheduleSelect.schedule_date.strftime('%Y-%m-%d %H:%M')
        
            a = Time.parse(trimDate)
            b = Time.parse(params[:booking_date])
            time = time_difference(b, a) / 60

            if cekScheduleBooking
                render json: {
                    message: 'you already have appoinment with the doctor for same schedule!',
                }, status: 200
            else
                if countBooking < 10
                    if b.to_date == a.to_date && time <= 30
                        @booking = Booking.new(booking_params)

                        if @booking.save
                            render json: @booking.to_json
                        else
                            render json: { error: @booking.errors }, status: 401
                        end
                    else
                        render json: {
                            message: 'The maximum user can book within 30 minutes before the doctor starts the schedule.',
                        }, status: 401
                    end
                else
                    render json: {
                        message: "Doctor already have 10 users for this schedule!",
                    }, status: 401
                end
            end
        end

        

    end

    def time_difference(time_a, time_b)
        difference = time_b - time_a
        
        if difference > 0
            difference
        else
            24 * 3600 + difference 
        end
    end

    private
    def booking_params
        params.permit(:user_id, :doctor_id, :hospital_id, :schedule_id, :booking_date, :active)
    end
end
