class UsersController < ApplicationController

    def index
        @user = User.where(level: 1)

        render json: @user.to_json
    end

    def create
        @user = User.new(user_params)

        if @user.save
            render json: @user.to_json
        else
            render json: { error: @user.errors }, status: 401
        end
    end

    private
    def user_params
        params.permit(:name, :email, :password, :password_confirmation, :level)
    end

end
