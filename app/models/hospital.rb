class Hospital < ApplicationRecord
    has_many :booking
    has_many :schedule

    validates_presence_of :name
    validates_presence_of :address
    validates_presence_of :phone
    validates_presence_of :open
end
