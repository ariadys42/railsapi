class Schedule < ApplicationRecord
    has_many :booking
    
    belongs_to :doctor, optional: true, class_name: 'User', foreign_key: 'doctor_id'
    belongs_to :hospital, optional: true, class_name: 'Hospital', foreign_key: 'hospital_id'
    validates :doctor_id, presence: true
    validates :hospital_id, presence: true
    validates :schedule_date, presence: true
end
