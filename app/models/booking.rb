class Booking < ApplicationRecord
    belongs_to :user, optional: true, class_name: 'User', foreign_key: 'user_id'
    belongs_to :doctor, optional: true, class_name: 'User', foreign_key: 'doctor_id'
    belongs_to :hospital, optional: true, class_name: 'Hospital', foreign_key: 'hospital_id'
    belongs_to :schedule, optional: true, class_name: 'Schedule', foreign_key: 'schedule_id'
    validates :user_id, presence: true
    validates :doctor_id, presence: true
    validates :hospital_id, presence: true
    validates :schedule_id, presence: true
    validates :booking_date, presence: true
    validates :active, presence: true
end
