class User < ApplicationRecord
    has_many :booking
    has_many :schedule

    has_secure_password
    validates_presence_of :email, uniqueness: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates_presence_of :name
    validates_presence_of :level
    validates_presence_of :password,
            length: { minimum: 6 },
            if: -> { new_record? || !password.nil? }
end
