require 'faker'

12.times do
    User.create(name: Faker::Name.name, email: Faker::Internet.email, password: Faker::Internet.password(min_length: 8))
end

5.times do
    User.create(name: Faker::Name.name, email: Faker::Internet.email, password: Faker::Internet.password(min_length: 8), level: 2)
end

10.times do
    Hospital.create(name: Faker::Company.name, address: Faker::Address.full_address, phone: Faker::PhoneNumber.phone_number, open: 'everyday')
end
