class CreateBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :bookings do |t|
      t.references :user, foreign_key: { to_table: :users }
      t.references :doctor, foreign_key: { to_table: :users }
      t.belongs_to :hospital, index: true, foreign_key: "hospital_id"
      t.belongs_to :schedule, index: true, foreign_key: "schedule_id"
      t.datetime :booking_date
      t.boolean :active, :default => true

      t.timestamps
    end
  end
end
