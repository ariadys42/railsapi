class CreateSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :schedules do |t|
      t.references :doctor, foreign_key: { to_table: :users }
      t.belongs_to :hospital, index: true, foreign_key: "hospital_id"
      t.datetime :schedule_date

      t.timestamps
    end
  end
end
