# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_16_050042) do

  create_table "bookings", force: :cascade do |t|
    t.integer "user_id"
    t.integer "doctor_id"
    t.integer "hospital_id"
    t.integer "schedule_id"
    t.datetime "booking_date"
    t.boolean "active", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["doctor_id"], name: "index_bookings_on_doctor_id"
    t.index ["hospital_id"], name: "index_bookings_on_hospital_id"
    t.index ["schedule_id"], name: "index_bookings_on_schedule_id"
    t.index ["user_id"], name: "index_bookings_on_user_id"
  end

  create_table "hospitals", force: :cascade do |t|
    t.string "name"
    t.text "address"
    t.string "phone"
    t.string "open"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.integer "doctor_id"
    t.integer "hospital_id"
    t.datetime "schedule_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["doctor_id"], name: "index_schedules_on_doctor_id"
    t.index ["hospital_id"], name: "index_schedules_on_hospital_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.integer "level", default: 1
    t.text "token"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "bookings", "hospitals"
  add_foreign_key "bookings", "schedules"
  add_foreign_key "bookings", "users"
  add_foreign_key "bookings", "users", column: "doctor_id"
  add_foreign_key "schedules", "hospitals"
  add_foreign_key "schedules", "users", column: "doctor_id"
end
