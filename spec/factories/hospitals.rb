FactoryBot.define do
    factory :hospital, class: Hospital do
      name { "RS SARI ASIH" }
      address  { "Jl. keselamatan No. 10" }
      phone { '123 456 789' }
      open { 'setiap hari' }
    end
end