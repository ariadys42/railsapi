FactoryBot.define do
    factory :schedule, class: Schedule do
      doctor_id { 1 }
      hospital_id  { 1 }
      schedule_date { "2020-10-16 10:30" }
    end
end