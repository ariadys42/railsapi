FactoryBot.define do
    factory :user, class: User do
      name { "test" }
      email  { "test@test.com" }
      password { 'test123' }
      level { 1 }
    end
end