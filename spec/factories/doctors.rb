FactoryBot.define do
    factory :doctor, class: User do
      name { "dr. John Doe" }
      email  { "doe@test.com" }
      password { 'test1234' }
      level { 2 }
    end
end