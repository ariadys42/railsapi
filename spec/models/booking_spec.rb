require 'rails_helper'

RSpec.describe Booking, type: :model do
  before do
    Hospital.count < 1 ? @valid_hospital = create(:hospital) : @valid_hospital = Hospital.first
    User.where(level: 2).count == 0 ? @valid_doctor = create(:doctor) : @valid_doctor = User.where(level: 2).first
    User.where(level: 1).count == 0 ? @valid_user = create(:user) : @valid_user = User.where(level: 1).first
    Schedule.count == 0 ? @valid_schedule = create(:schedule) : @valid_schedule = Schedule.first
  end
  subject { 
    described_class.new(
      user_id: @valid_user.id,
      doctor_id: @valid_doctor.id,
      hospital_id: @valid_hospital.id,
      schedule_id: @valid_schedule.id,
      booking_date: Faker::Time.between(from: DateTime.now - 1, to: DateTime.now),
      active: true) 
  }
  
  context 'validation test' do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
    it "is not valid without a user_id" do
      subject.user_id = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a doctor_id" do
      subject.doctor_id = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a hospital_id" do
      subject.hospital_id = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a schedule_id" do
      subject.schedule_id = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a booking_date" do
      subject.booking_date = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a active" do
      subject.active = nil
      expect(subject).to_not be_valid
    end
    
  end
end
