require 'rails_helper'

RSpec.describe User, type: :model do
  subject { 
    described_class.new(
      name: "test",
      email: "test@test.com",
      password: "test123",
      level: 1) 
  }
  it {should have_many (:booking)}
  context 'validation test' do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a email" do
      subject.email = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a password" do
      subject.password = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a level" do
      subject.level = nil
      expect(subject).to_not be_valid
    end
    describe "when email format is invalid" do
      it "should be invalid" do
        addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                       foo@bar_baz.com foo@bar+baz.com]
        addresses.each do |invalid_address|
          subject.email = invalid_address
          expect(subject).not_to be_valid
        end 
      end
    end
    describe "when email format is valid" do
      it "should be valid" do
        addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
        addresses.each do |valid_address|
          subject.email = valid_address
          expect(subject).to be_valid
        end
      end
    end
  end
end
