require 'rails_helper'

RSpec.describe Hospital, type: :model do
  subject { 
    described_class.new(
      name: 'RS TEST',
      address: 'Jl. Test',
      phone: '021 7123 456',
      open: 'everyday') 
  }
  it {should have_many (:booking)}
  it {should have_many (:schedule)}
  context 'validation test' do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a address" do
      subject.address = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a phone" do
      subject.phone = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a open" do
      subject.open = nil
      expect(subject).to_not be_valid
    end
  end
end
