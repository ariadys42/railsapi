require 'rails_helper'

RSpec.describe Schedule, type: :model do
  before do
    Hospital.count < 1 ? @valid_hospital = create(:hospital) : @valid_hospital = Hospital.first
    User.where(level: 2).count == 0 ? @valid_doctor = create(:doctor) : @valid_doctor = User.where(level: 2).first
  end
  subject { 
    described_class.new(
      doctor_id: @valid_doctor.id,
      hospital_id: @valid_hospital.id,
      schedule_date: Faker::Time.between(from: DateTime.now - 1, to: DateTime.now))
  }
  context 'validation test' do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end
    it "is not valid without a doctor_id" do
      subject.doctor_id = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a hospital_id" do
      subject.hospital_id = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without a schedule_date" do
      subject.schedule_date = nil
      expect(subject).to_not be_valid
    end
  end
end
