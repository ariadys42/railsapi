require 'rails_helper'

RSpec.describe ScheduleController, type: :controller do
    before do
        User.where(level: 2).count == 0 ? @valid_doctor = create(:doctor) : @valid_doctor = User.where(level: 2).first
        Hospital.count < 1 ? @valid_hospital = create(:hospital) : @valid_hospital = Hospital.first
        @token_new = JsonToken.encode(user_id: @valid_doctor.id)
    end
    describe "GET #index with auth" do
        before do 
            if Schedule.count == 0
                headers = { :Authorization => @token_new }
                request.headers.merge! headers
                post :create, params:{:doctor_id => @valid_doctor.id, :hospital_id => @valid_hospital.id, :schedule_date => '2020-10-20 10:30'}
            end
        end
        it "returns a 200" do
            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            get :index
            expect(response).to have_http_status(200)
        end
    end
    describe "GET #index without auth" do
        it "returns a 401" do
            get :index
            expect(response).to have_http_status(401)
        end
    end
    describe "POST #create with auth" do
        it "is valid data" do
            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            post :create, params:{:doctor_id => @valid_doctor.id, :hospital_id => @valid_hospital.id, :schedule_date => Faker::Time.between(from: DateTime.now - 1, to: DateTime.now)}
            expect(response).to have_http_status(200)
        end
        it "is invalid data" do
            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            post :create, params:{:doctor_id => @valid_doctor.id, :hospital_id => 0, :schedule_date => '2020-10-21 10:22'}
            expect(response).to have_http_status(401)
        end
        it "is empty data" do
            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            post :create, params: {}
            expect(response).to have_http_status(401)
        end
    end
    describe "POST #create without auth" do
        it "is valid data" do
            post :create, params:{:doctor_id => @valid_doctor.id, :hospital_id => @valid_hospital.id, :schedule_date => '2020-10-20 10:30'}
            expect(response).to have_http_status(401)
        end
        it "is invalid data" do
            post :create, params:{:doctor_id => @valid_doctor.id, :hospital_id => @valid_hospital.id, :schedule_date => ''}
            expect(response).to have_http_status(401)
        end
        it "is valid data" do
            post :create, params:{}
            expect(response).to have_http_status(401)
        end
    end
end
