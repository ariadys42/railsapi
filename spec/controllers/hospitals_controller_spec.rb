require 'rails_helper'

RSpec.describe HospitalsController, type: :controller do
    describe "GET #hospitals" do
        before do
            Hospital.count < 1 ? @valid_hospital = create(:hospital) : @valid_hospital = Hospital.first
        end
        it "return all hospital" do
            get :index
            expect(response).to have_http_status(200)
            expect(JSON.parse(response.body).size).to eq(1)
        end
    end
end
