require 'rails_helper'

RSpec.describe AuthenticationController, type: :controller do
    before do
        User.where(email: 'doe@test.com').count == 0 ? @valid_user = create(:doctor) : @valid_user = User.where(email: 'doe@test.com').first
    end
    describe "user" do
        it "is valid data" do
          post "login", params: {:email => @valid_user.email, :password => 'test1234'}
          json = JSON.parse(response.body)
          expect(response).to have_http_status(200)
          expect(json['token']).not_to be_nil
        end

        it "is invalid data" do
            post "login", params: {:email => @valid_user.email, :password => '123'}
            expect(response).to have_http_status(401)
        end

        it "is empty data" do
            post "login", params:{}
            expect(response).to have_http_status(401)
        end
    end
end
