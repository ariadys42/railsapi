require 'rails_helper'

RSpec.describe BookingController, type: :controller do
    before do
        Hospital.count < 1 ? @valid_hospital = create(:hospital) : @valid_hospital = Hospital.first
        User.where(level: 2).count == 0 ? @valid_doctor = create(:doctor) : @valid_doctor = User.where(level: 2).first
        User.where(level: 1).count == 0 ? @valid_user = create(:user) : @valid_user = User.where(level: 1).order("RANDOM()").first
        Schedule.count == 0 ? @valid_schedule = create(:schedule) : @valid_schedule = Schedule.first
        @token_new = JsonToken.encode(user_id: @valid_user.id)
    end
    describe "GET #index with auth" do
        it "returns a 200" do
            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            get :index
            expect(response).to have_http_status(200)
        end
    end
    describe "GET #index without auth" do
        it "returns a 401" do
            get :index
            expect(response).to have_http_status(401)
        end
    end
    describe "POST #create with auth" do
        it "is valid data" do
            trimDate = @valid_schedule.schedule_date.strftime('%Y-%m-%d %H:%M')
        
            a = Time.parse(trimDate)

            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            post :create, params: {
                :user_id => @valid_user.id, 
                :doctor_id => @valid_schedule.doctor_id, 
                :hospital_id => @valid_hospital.id, 
                :schedule_id => @valid_schedule.id,
                :booking_date =>  a - 10, 
                :active => true }
            expect(response).to have_http_status(200)
        end
        it "is invalid data" do
            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            post :create, params:{:user_id => @valid_user.id, :doctor_id => @valid_doctor.id, 
            :hospital_id => '', 
            :schedule_id => @valid_schedule.id,
            :booking_date => '2020-10-16 10:10', 
            :active => true}
            expect(response).to have_http_status(401)
        end
        it "is started schedule" do
            trimDate = @valid_schedule.schedule_date.strftime('%Y-%m-%d %H:%M')
        
            a = Time.parse(trimDate)

            headers = { :Authorization => @token_new }
            request.headers.merge! headers
            post :create, params: {
                :user_id => @valid_user.id, 
                :doctor_id => @valid_schedule.doctor_id, 
                :hospital_id => @valid_hospital.id, 
                :schedule_id => @valid_schedule.id,
                :booking_date =>  a + 5, 
                :active => true }
            expect(response).to have_http_status(401)
        end
    end
end
