require 'rails_helper'

RSpec.describe DoctorsController, type: :controller do
    describe "GET #doctors" do
        before do
            User.where(level: 2).count == 0 ? @valid_doctor = create(:doctor) : @valid_doctor = User.where(level: 2).first
        end
        it "return all doctor" do
            get :index
            expect(response).to have_http_status(200)
            expect(JSON.parse(response.body).size).to eq(1)
        end
    end
end
