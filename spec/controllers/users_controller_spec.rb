require 'rails_helper'

RSpec.describe UsersController, type: :controller do
    describe "GET #index" do
        before do
            User.where(level: 1).count == 0 ? @user = create(:user) : @user = User.where(level: 1).first
        end
        it "return all user" do
            get :index
            expect(response).to have_http_status(200)
        end
    end
    describe "POST #create" do
        it "is valid data" do
            post :create, params:{:name => Faker::Name.name, :email => Faker::Internet.email, :password => '12345678', :password_confirmation => '12345678', :level => 1}
            json = JSON.parse(response.body)
            expect(response).to have_http_status(200)
        end
        it "is invalid data" do
            post :create, params:{:name => 'John Doe', :email => 'john', :password => '12345678', :password_confirmation => '12345678'}
            expect(response).to have_http_status(401)
        end
        it "is empty param" do
            post :create, params:{}
            expect(response).to have_http_status(401)
        end
    end
end
